const lowercaseletters="abcdefghijklmnopqrstuvwxyz";
const uppercaseletters="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
const numbers="1234567890";
const symbols="!@#$%^&*()_+=-[]{}\\|;':\",./<>?~`";

const length=document.getElementById("length");
const lowercase=document.getElementById("lowercase");
const uppercase=document.getElementById("uppercase");
const number=document.getElementById("numbers");
const symbol=document.getElementById("symbols");
const generateBtn=document.getElementById("generate");
const result=document.getElementById("password");

generateBtn.addEventListener("click",function(){
    const len=length.value;
    let characters="";
    let password="";
    if(lowercase.checked){
        characters+=lowercaseletters;
    }
    if(uppercase.checked){
        characters+=uppercaseletters;
    }
    if(number.checked){
        characters+=numbers;
    }
    if(symbol.checked){
        characters+=symbols;
    }   
    for(let i=0;i<len;i++){
        password+=characters.charAt(Math.floor(Math.random() * characters.length));
    }
    result.value=password;
});