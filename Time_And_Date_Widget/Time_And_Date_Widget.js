let dateContainer=document.querySelector(".date-container");
let hoursContainer=document.querySelector(".hours");
let minsContainer=document.querySelector(".mins");
let secsContainer=document.querySelector(".secs");

const weekdays=["Sunday", "Monday", "Tuesday", "Wednesday", "Thrusday", "Friday", "Saturday"];

const monthsNAmes=["january", "February", "March", "April", "May" ,"June", "July", "August", "September", "October", "November", "December"];

function formateTime(time){
    return time < 10? "0"+time : time;
}
function updateClock(){
    const today=new Date();
    let date=today.getDate();
    let day=weekdays[today.getDay()];
    let month=monthsNAmes[today.getMonth()];
    let hours=formateTime(today.getHours());
    let mins=formateTime(today.getMinutes());
    let secs=formateTime(today.getSeconds());

    dateContainer.innerHTML=`
        <p>${day}</p><p><span>${date}</span></p><p>${month}</p>
    `;
    hoursContainer.textContent=hours+":";
    minsContainer.textContent=mins+":";
    secsContainer.textContent=secs;
}
setInterval(updateClock,1000);
updateClock();