<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"
    import="java.util.*, Model.Todo, JDBC.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Update Todo</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <style>
        header{
            background-color: tomato;
            border: 1px solid;
            display: flex;
            justify-content: space-between;
        }
        .first_div{
            display: flex;
        }
        a{
            text-decoration: none;
            color: black;
        }
        .second_div{
            margin:100px auto;
            box-shadow: 0px 3px 10px rgba(0, 0, 0, 0.3);
        }
    </style>
</head>
<body>
	
	<header>
        <div class="first_div">
            <p style="font-size: 30px; margin-left: 15px; margin-top: 10px;">TODO List</p>
            <p style="font-size: 20px; margin-left: 20px; margin-top: 20px;"><a href="TODO_List.jsp">TODOs</a></p>
        </div>
        <div>
            <p style="font-size: 20px; margin-right: 20px; margin-top: 20px;"><a href="Logout">Logout</a></p>
        </div>
    </header>
    <div class="container col-md-5 second_div">
        <form action="update" method="post">
        <% 
		int id=Integer.parseInt(request.getParameter("ID"));
		Todo td=Todo_Data_Management.retriveData(id);
		session.setAttribute("id",id); %>
        	<h2 style="padding: 10px;">Update Todo</h2>
            <div style="padding: 10px;">
                <label for="Todo_Title" class="form-label fs-5">Todo Title</label>
                <input type="text" id="Todo_Title" class="form-control" name="title" value="<%=td.getTitle()%>" required>
            </div>
            <div style="padding: 10px;">
                <label for="Todo_Status" class="form-label fs-5">Todo Status</label>
                <select name='status' id='Todo_Status' class='form-select'>
					<option value="In Progress" <%=td.getStatus().equals("In Progress")?"selected":"" %>>In Progress</option>
					<option value="Completed" <%=td.getStatus().equals("Completed")?"selected":"" %>>Completed</option>
				</select>
            </div>
            <div style="padding: 10px;">
                <label for="Todo_Target_Date" class="form-label fs-5">Todo Target Date</label>
                <input type="date" id="Todo_Target_Date" class="form-control" name="date" value="<%=td.getDate()%>" required>
            </div>
            <div style="padding: 10px;">
                <input type="submit" class="btn btn-success" value="Update Todo">
            </div> 
        </form>
    </div>
</body>
</html>