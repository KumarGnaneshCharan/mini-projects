<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"
    import="java.util.*, Model.Todo, JDBC.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>ToDO List</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <style>
        header{
            background-color: tomato;
            border: 1px solid;
            display: flex;
            justify-content: space-between;
        }
        .first_div{
            display: flex;
        }
        a{
            text-decoration: none;
            color: black;
        }
    </style>
</head>
<body>
    <header>
        <div class="first_div">
            <p style="font-size: 30px; margin-left: 15px; margin-top: 10px;">TODO List</p>
            <p style="font-size: 20px; margin-left: 20px; margin-top: 20px;"><a href="TODO_List.jsp">TODOs</a></p>
        </div>
        <div>
            <p style="font-size: 20px; margin-right: 20px; margin-top: 20px;"><a href="Logout">Logout</a></p>
        </div>
    </header>
    <% 
    	int user_id=(int) session.getAttribute("user_id");
    	int count=1;
    	List<Todo> list = Todo_Data_Management.retriveList(user_id);
    %>
    <div>
        <h2 class="container text-center" style="margin: 10px auto;">List of Todos</h2>
        <hr>
        <a href="addTodo" class="btn btn-success" style="margin: 10px 0 0 50px;">Add Todo</a>
    </div>
    <div style="display: flex; justify-content: center; margin: 50px auto;">
        <table class="table-bordered text-center">
            <tr style="height: 40px;">
                <th style="width:50px;">S.No</th>
                <th style="width:250px;">Title</th>
                <th style="width:120px;">Target Date</th>
                <th style="width:100px;">Todo Status</th>
                <th style="width:210px;">Action</th>
            </tr>
            <% for(Todo td: list){ %>
            <tr>
            	<td><%=count++%></td>
            	<td><%=td.getTitle()%></td>
            	<td><%=td.getStatus()%></td>
            	<td><%=td.getDate()%></td>
            	<td>
                    <form action="UpdateTodo.jsp" method="post" style="display: inline;">
                      <input type="hidden" name="ID" value="<%= td.getId() %>">
                      <input type="submit" value="Update" class="btn" style="color:blue">
                    </form>
                    <form action="delete" method="post" style="display: inline;">
                      <input type="hidden" name="ID" value="<%= td.getId() %>">
                      <input type="submit" value="Delete" class="btn" style="color:blue">
                    </form>
                </td>
            </tr>
            <%}%>
        </table>
    </div>
</body>
</html>