package JDBC;
import java.sql.*;
import Model.*;
public class User_Data_Management {
	public static int User_Registration(TODO_List td){
		Connection con=null;
		int result=0;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			con=DriverManager.getConnection("jdbc:mysql://localhost:3306/TODO_List","root","root");
			String query="insert into users (First_Name,Last_Name,gender,Email,Password) values(?,?,?,?,?)";
			PreparedStatement ps=con.prepareStatement(query);
			ps.setString(1, td.getFirst_Name());
			ps.setString(2, td.getLast_Name());
			ps.setString(3, td.getGender());
			ps.setString(4, td.getEmail());
			ps.setString(5, td.getPassword());
			result=ps.executeUpdate();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	public static int User_Login(TODO_List td){
		Connection con=null;
		int user_id=0;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			con=DriverManager.getConnection("jdbc:mysql://localhost:3306/TODO_List","root","root");
			String query="select * from users where email=? and password=?";
			PreparedStatement ps=con.prepareStatement(query);
			ps.setString(1, td.getEmail());
			ps.setString(2, td.getPassword());
			ResultSet rs=ps.executeQuery();
			if(rs.next()){
				user_id=rs.getInt(1);
			}
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		return user_id;
	}
	public static boolean ForgotPassword(String email){
		Connection con=null;
		boolean result=false;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			con=DriverManager.getConnection("jdbc:mysql://localhost:3306/TODO_List","root","root");
			String query="select * from users where email=?";
			PreparedStatement ps=con.prepareStatement(query);
			ps.setString(1, email);
			ResultSet rs=ps.executeQuery();
			result=rs.isBeforeFirst();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	public static void ChangePassword(String email,String password){
		Connection con=null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			con=DriverManager.getConnection("jdbc:mysql://localhost:3306/TODO_List","root","root");
			String query="update users set password=? where email=?";
			PreparedStatement ps=con.prepareStatement(query);
			ps.setString(1, password);
			ps.setString(2,email);
			ps.execute();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}
}
