package JDBC;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import Model.Todo;
public class Todo_Data_Management {
	public static void InsertTodo(Todo todo){
		Connection con=null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			con=DriverManager.getConnection("jdbc:mysql://localhost:3306/todo_list","root","root");
			String query="insert into todo_content (title,date,status,user_id) values (?,?,?,?)";
			PreparedStatement ps=con.prepareStatement(query);
			ps.setString(1,todo.getTitle());
			ps.setDate(2,todo.getDate());
			ps.setString(3,todo.getStatus());
			ps.setInt(4, todo.getUser_id());
			ps.executeUpdate();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}
	public static ArrayList<Todo> retriveList(int user_id){
		ArrayList<Todo> list=new ArrayList();
		Connection con=null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			con=DriverManager.getConnection("jdbc:mysql://localhost:3306/todo_list","root","root");
			String query="select * from todo_content where user_id=?";
			PreparedStatement ps=con.prepareStatement(query);
			ps.setInt(1,user_id);
			ResultSet rs=ps.executeQuery();
			if(rs.isBeforeFirst()){
				while(rs.next()){
					Todo td=new Todo(rs.getInt(1),rs.getString(2),rs.getDate(3),rs.getString(4));
					list.add(td);
				}
			}
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	public static Todo retriveData(int id){
		Todo td=null;
		Connection con=null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			con=DriverManager.getConnection("jdbc:mysql://localhost:3306/todo_list","root","root");
			String query="select * from todo_content where id=?";
			PreparedStatement ps=con.prepareStatement(query);
			ps.setInt(1,id);
			ResultSet rs=ps.executeQuery();
			if(rs.isBeforeFirst()){
				while(rs.next()){
					td=new Todo(rs.getString(2),rs.getDate(3),rs.getString(4));
				}
			}
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		return td;
	}
	public static void updateData(Todo td){
		Connection con=null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			con=DriverManager.getConnection("jdbc:mysql://localhost:3306/todo_list","root","root");
			String query="update todo_content set title=?,status=?,date=? where id=?";
			PreparedStatement ps=con.prepareStatement(query);
			ps.setString(1,td.getTitle());
			ps.setString(2,td.getStatus());
			ps.setDate(3,td.getDate());
			ps.setInt(4,td.getId());
			ps.executeUpdate();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}
	public static void deleteData(int id){
		Connection con=null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			con=DriverManager.getConnection("jdbc:mysql://localhost:3306/todo_list","root","root");
			String query="delete from todo_content where id=?";
			PreparedStatement ps=con.prepareStatement(query);
			ps.setInt(1,id);
			ps.execute();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}
}
