package Model;

import java.sql.Date;
import java.time.LocalDate;

public class Todo {
	private int id;
	private String title;
	private Date date;
	private String status;
	private int user_id;
	public Todo(String title, Date date, String status,int user_id) {
		this.title = title;
		this.date = date;
		this.status = status;
		this.user_id=user_id;
	}
	public Todo(String title, Date date2, String status) {
		this.title = title;
		this.date = date2;
		this.status = status;
	}
	public Todo(int id, String title, Date date, String status) {
		this.id = id;
		this.title = title;
		this.date = date;
		this.status = status;
	}
	public Todo() {
		
	}
	public int getId() {
		return id;
	}
	public String getTitle() {
		return title;
	}
	public Date getDate() {
		return date;
	}
	public String getStatus() {
		return status;
	}
	public int getUser_id() {
		return user_id;
	}
	@Override
	public String toString() {
		return "Todo [id=" + id + ", title=" + title + ", date=" + date + ", status=" + status + "]";
	}
}
