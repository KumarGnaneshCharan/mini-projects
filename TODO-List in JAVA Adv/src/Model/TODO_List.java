package Model;

public class TODO_List {
	private int id;
	private String First_Name;
	private String Last_Name;
	private String gender;
	private String email;
	private String password;
	public TODO_List(){
		
	}
	public TODO_List(String first_Name, String last_Name, String gender, String email, String password) {
		First_Name = first_Name;
		Last_Name = last_Name;
		this.gender = gender;
		this.email = email;
		this.password = password;
	}
	public TODO_List(int id, String first_Name, String last_Name, String gender, String email, String password) {
		this.id = id;
		First_Name = first_Name;
		Last_Name = last_Name;
		this.gender = gender;
		this.email = email;
		this.password = password;
	}
	public TODO_List(String email, String password) {
		this.email = email;
		this.password = password;
	}
	public int getId() {
		return id;
	}
	public String getFirst_Name() {
		return First_Name;
	}
	public String getLast_Name() {
		return Last_Name;
	}
	public String getGender() {
		return gender;
	}
	public String getEmail() {
		return email;
	}
	public String getPassword() {
		return password;
	}
	@Override
	public String toString() {
		return "TODO_List [id=" + id + ", First_Name=" + First_Name + ", Last_Name=" + Last_Name + ", gender=" + gender
				+ ", email=" + email + ", password=" + password + "]";
	}
}
