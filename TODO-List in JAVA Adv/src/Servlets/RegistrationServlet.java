package Servlets;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import JDBC.*;
import Model.TODO_List;

/**
 * Servlet implementation class RegistrationServlet
 */
@WebServlet("/Registration")
public class RegistrationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		res.sendRedirect("Registration_Page.html");
	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Register(request, response);
	}
	
	private void Register(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		String F_Name=req.getParameter("f_name");
		String L_Name=req.getParameter("l_name");
		String gender=req.getParameter("gender");
		String email=req.getParameter("email");
		String password=req.getParameter("password");
		TODO_List td=new TODO_List(F_Name,L_Name,gender,email,password);
		if(!F_Name.trim().isEmpty() && !L_Name.trim().isEmpty() && !email.trim().isEmpty() && !password.trim().isEmpty()){
			int result=User_Data_Management.User_Registration(td);
			if(result==1){
				RequestDispatcher rd= req.getRequestDispatcher("Login_Page.html");
				rd.forward(req,res);
			}
			else{
				req.getRequestDispatcher("Fail1.html").forward(req, res);
			}
		}
		else{
			req.getRequestDispatcher("Fail1.html").forward(req, res);
		}
	}
}
