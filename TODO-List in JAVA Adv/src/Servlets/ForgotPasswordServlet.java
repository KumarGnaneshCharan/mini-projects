package Servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import JDBC.User_Data_Management;

/**
 * Servlet implementation class ForgotPasswordServlet
 */
@WebServlet("/ForgotPassword")
public class ForgotPasswordServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ForgotPassword(request,response);
	}

	private void ForgotPassword(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		String email=req.getParameter("email");
		if(User_Data_Management.ForgotPassword(email)){
			HttpSession session =req.getSession();
			session.setAttribute("email",email);
			RequestDispatcher rd=req.getRequestDispatcher("ChangePassword.html");
			rd.forward(req, res);
		}
		else{
			RequestDispatcher rd=req.getRequestDispatcher("Fail3.html");
			rd.forward(req, res);
		}
	}

}
