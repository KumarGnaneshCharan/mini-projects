package Servlets;
import JDBC.*;
import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Model.TODO_List;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/Login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("Login_Page.html");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Login(request, response);
	}

	private void Login(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		String email=req.getParameter("email");
		String password=req.getParameter("password");
		TODO_List td=new TODO_List(email,password);
		int user_id=User_Data_Management.User_Login(td);
		if(user_id!=0){
			HttpSession session=req.getSession();
			session.setAttribute("user_id", user_id);
			RequestDispatcher rd=req.getRequestDispatcher("TODO_List.jsp");
			rd.forward(req,res);
		}
		else{
			req.getRequestDispatcher("Fail2.html").forward(req,res);;
		}
	}

}
