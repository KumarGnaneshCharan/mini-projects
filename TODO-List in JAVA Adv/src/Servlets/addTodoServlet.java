package Servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import JDBC.Todo_Data_Management;
import Model.Todo;

/**
 * Servlet implementation class formAddServlet
 */
@WebServlet("/addTodo")
public class addTodoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("todoAdd.html");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AddList(request, response);
	}
	
	protected void AddList(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{
		String title= req.getParameter("title");
		String Date=req.getParameter("date");
		String status=req.getParameter("Status");
		SimpleDateFormat formate=new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date date=null;
		java.sql.Date sqlDate=null;
		try {
			date = formate.parse(Date);
			sqlDate = new java.sql.Date(date.getTime());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		HttpSession session=req.getSession();
		int user_id=(int)session.getAttribute("user_id");
		Todo td=new Todo(title,sqlDate,status,user_id);
		Todo_Data_Management.InsertTodo(td);
		RequestDispatcher rd=req.getRequestDispatcher("TODO_List.jsp");
		rd.forward(req,res);
	}
}
