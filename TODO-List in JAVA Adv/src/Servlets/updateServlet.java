package Servlets;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import JDBC.Todo_Data_Management;
import Model.Todo;

/**
 * Servlet implementation class updateServlet
 */
@WebServlet("/update")
public class updateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		UpdateData(request,response);
	}

	private void UpdateData(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		String title=req.getParameter("title");
		String status=req.getParameter("status");
		String Date=req.getParameter("date");
		SimpleDateFormat formate=new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date date=null;
		java.sql.Date sqlDate=null;
		try {
			date = formate.parse(Date);
			sqlDate = new java.sql.Date(date.getTime());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		HttpSession session=req.getSession();
		int id=(int) session.getAttribute("id");
		Todo td=new Todo(id,title,sqlDate,status);
		Todo_Data_Management.updateData(td);
		RequestDispatcher rd=req.getRequestDispatcher("TODO_List.jsp");
		rd.forward(req,res);
	}

}
