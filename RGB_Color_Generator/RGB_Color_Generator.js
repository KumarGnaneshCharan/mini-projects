const redslider=document.getElementById("redslider");
const greenslider=document.getElementById("greenslider");
const blueslider=document.getElementById("blueslider");

const redspanvalue=document.getElementById("redvalue");
const greenspanvalue=document.getElementById("greenvalue");
const bluespanvalue=document.getElementById("bluevalue");

const colorbox=document.getElementById("color-box");
const button=document.getElementById("btn");
const input_RGB_Value=document.getElementById("inputtype");

redslider.addEventListener("input",updatecolor);
greenslider.addEventListener("input",updatecolor);
blueslider.addEventListener("input",updatecolor);
button.addEventListener("click",copyRGB_Value);

document.addEventListener("DOMContentLoaded",function(){
    redslider.value=0;
    greenslider.value=0;
    blueslider.value=0;
})


function updatecolor(){
    const redvalue=redslider.value;
    const greenvalue=greenslider.value;
    const bluevalue=blueslider.value;
    const rgbValue=`rgb(${redvalue}, ${greenvalue}, ${bluevalue})`;
    colorbox.style.backgroundColor=rgbValue;
    redspanvalue.textContent=redvalue;
    greenspanvalue.textContent=greenvalue;
    bluespanvalue.textContent=bluevalue;
    input_RGB_Value.textContent=rgbValue;

}

function copyRGB_Value(){
    const redvalue=redslider.value;
    const greenvalue=greenslider.value;
    const bluevalue=blueslider.value;
    const rgbValue=`rgb(${redvalue}, ${greenvalue}, ${bluevalue})`;

    navigator.clipboard.writeText(rgbValue)
        .then(()=>{
            alert("RGB Color Value  Copied to Clipboard: "+rgbValue);
        })
        .catch((error)=>{
            console.error("Failed to copy RGB value",error);
        })
}