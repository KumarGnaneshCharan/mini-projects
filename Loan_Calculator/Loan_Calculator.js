function calculate(){
    var loan=parseFloat(document.getElementById("loan").value);
    var interest=parseFloat(document.getElementById("interest").value);
    var term=parseFloat(document.getElementById("term").value);
    if(isNaN(loan)||isNaN(interest)||isNaN(term)){
        alert("Enter All Fields");
    }
    else{
        const monthlyInterest=interest/100/12;
        const monthlyPayment=(loan*monthlyInterest)/(1-Math.pow(1+monthlyInterest,-term));
        const totalInterest=(monthlyPayment*term)-loan;
        displayResult(monthlyPayment,totalInterest);
    }
}
function displayResult(monthlyPayment,totalInterest){
    var result=document.getElementById("result");
    result.innerHTML=`
    <p><strong>Monthly Payment: ${monthlyPayment.toFixed(2)}</strong></p>
    <p><strong>Total interest: ${totalInterest.toFixed(2)}</strong></p>
    `;
}