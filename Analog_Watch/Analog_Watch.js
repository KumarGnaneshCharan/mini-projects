const body=document.querySelector("body");
const hourHand=document.querySelector(".hour");
const minHand=document.querySelector(".min");
const secHand=document.querySelector(".sec");
const modeSwitch=document.querySelector(".mode-switch");

if(localStorage.getItem("mode")==="Dark Mode"){
    body.classList.add("dark");
    modeSwitch.textContent="Light Mode";
}
modeSwitch.addEventListener("click",()=>{
    body.classList.toggle("dark");
    const isDarkMode=body.classList.contains("dark");
    modeSwitch.textContent=isDarkMode?"Light Mode":"Dark Mode";
    localStorage.setItem("mode",isDarkMode?"Light Mode":"Dark Mode");
})

const updateTime=()=>{
    let date=new Date();
        sectodeg=(date.getSeconds()/60)*360,
        mintodeg=(date.getMinutes()/60)*360,
        hourtodeg=(date.getHours()/12)*360;
    secHand.style.transform=`rotate(${sectodeg}deg)`;
    minHand.style.transform=`rotate(${mintodeg}deg)`;
    hourHand.style.transform=`rotate(${hourtodeg}deg)`;
};

setInterval(updateTime,1000);
updateTime();
