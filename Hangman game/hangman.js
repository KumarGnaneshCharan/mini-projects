// Retrieve background image URL from local storage and set it as the background
document.addEventListener("DOMContentLoaded",function(){
    var bgimageurl=getBackgroundImage();
    if(bgimageurl){
        document.body.style.backgroundImage="url('"+bgimageurl+"')";
    }
})
// Function to set the background image URL in local storage
function setBackgroundImage(imageURL){
    localStorage.setItem('bgimageurl',imageURL);
}
// Function to get the background image URL from local storage
function getBackgroundImage(){
    return localStorage.getItem('bgimageurl');
}
function blue(){
    body=document.querySelector("body");
    body.style.backgroundImage="url('background-blue.png')"
    setBackgroundImage('background-blue.png')
}
function pink(){
    body=document.querySelector("body");
    body.style.backgroundImage="url('background-pink.png')"
    setBackgroundImage('background-pink.png');
}
/* --------------------------------------------------------------- */
function mix(){
    pr=document.getElementById("print");
    fetch('Default.txt')
    .then(response => response.text())
    .then(data => {
        let mixArray=data.split('\n').filter(Boolean);
        let randomIndex=Math.floor(Math.random() * mixArray.length);
        let randomMix=mixArray[randomIndex];
        setRandomWord(randomMix);
        let theme_name="Mixed";
        setThemename(theme_name);
        window.location.href='difficulty.html';
    })
}
function animal(){
    fetch('Animal.txt')
    .then(response => response.text())
    .then(data => {
        let animalArray=data.split('\n').filter(Boolean);
        let randomeIndex=Math.floor(Math.random() * animalArray.length);
        let randomAnimal=animalArray[randomeIndex];
        setRandomWord(randomAnimal);
        let theme_name="Animal";
        setThemename(theme_name);
        window.location.href='difficulty.html';
    })
}
function country(){
    fetch('Country.txt')
    .then(response => response.text())
    .then(data => {
        let countryArray=data.split('\n').filter(Boolean);
        let randomeIndex=Math.floor(Math.random() * countryArray.length);
        let randomCountry=countryArray[randomeIndex];
        setRandomWord(randomCountry);
        let theme_name="Country";
        setThemename(theme_name);
        window.location.href='difficulty.html';
    })
}
function nature(){
    fetch('Nature.txt')
    .then(response => response.text())
    .then(data => {
        let natureArray=data.split('\n').filter(Boolean);
        let randomeIndex=Math.floor(Math.random() * natureArray.length);
        let randomNature=natureArray[randomeIndex];
        setRandomWord(randomNature);
        let theme_name="Nature";
        setThemename(theme_name);
        window.location.href='difficulty.html';
    })
}
function sport(){
    fetch('Sport.txt')
    .then(response => response.text())
    .then(data => {
        let sportArray=data.split('\n').filter(Boolean);
        let randomeIndex=Math.floor(Math.random() * sportArray.length);
        let randomSport=sportArray[randomeIndex];
        setRandomWord(randomSport);
        let theme_name="Sport";
        setThemename(theme_name);
        window.location.href='difficulty.html';
    })
}
function occupation(){
    fetch('Occupation.txt')
    .then(response => response.text())
    .then(data => {
        let occupationArray=data.split('\n').filter(Boolean);
        let randomeIndex=Math.floor(Math.random() * occupationArray.length);
        let randomOccupation=occupationArray[randomeIndex];
        setRandomWord(randomOccupation);
        let theme_name="Occupation";
        setThemename(theme_name);
        window.location.href='difficulty.html';
    })
}
function food(){
    fetch('Food.txt')
    .then(response => response.text())
    .then(data => {
        let foodArray=data.split('\n').filter(Boolean);
        let randomeIndex=Math.floor(Math.random() * foodArray.length);
        let randomFood=foodArray[randomeIndex];
        setRandomWord(randomFood);
        let theme_name="Food";
        setThemename(theme_name);
        window.location.href='difficulty.html';
    })
}
function fruit(){
    fetch('Fruit.txt')
    .then(response => response.text())
    .then(data => {
        let fruitArray=data.split('\n').filter(Boolean);
        let randomeIndex=Math.floor(Math.random() * fruitArray.length);
        let randomFruit=fruitArray[randomeIndex];
        setRandomWord(randomFruit);
        let theme_name="Fruit";
        setThemename(theme_name);
        window.location.href='difficulty.html';
    })
}
// Function to set the random word in local storage
function setRandomWord(randomWord){
    localStorage.setItem('randomWord',randomWord);
}
// Function to get the random word from local storage
function getRandomWord(){
    return localStorage.getItem('randomWord');
}
function setThemename(theme_name){
    localStorage.setItem('theme_name',theme_name);
}
function getThemename(){
    return localStorage.getItem('theme_name');
}
/* --------------------------------------------------------------- */
function easy(){
    var easy=document.getElementById("easy").value;
    setDifficulty(easy);
    window.location.href='final_page.html';
}
function medium(){
    var medium=document.getElementById("medium").value;
    setDifficulty(medium);
    window.location.href='final_page.html';
}
function hard(){
    var hard=document.getElementById("hard").value;
    setDifficulty(hard);
    window.location.href='final_page.html';
}
function setDifficulty(level){
    localStorage.setItem('level',level);
}
function getDifficulty(){
    return localStorage.getItem('level');
}
/* --------------------------------------------------------------- */
function popup(){
    var x=document.getElementById("overlay");
    var y=document.getElementById("popup");
    x.style.display="block";
    y.style.opacity="1";
}
function yes(){
    window.location.href="firstpage.html";
}
function no(){
    var x=document.getElementById("overlay");
    var y=document.getElementById("popup");
    x.style.display="none";
    y.style.opacity="0";
}
