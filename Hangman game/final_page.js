let count=0;
        let arr=[];
        let guessedletter=[];
        let wrongletter=[];
        let a=document.getElementById("word_dashes");
        let b=document.getElementById("wrong_letters");
        let d=parseInt(getDifficulty());
        let c=getRandomWord();
        let overlay=document.getElementById("overlay");
        let Gameend=document.getElementById("gameend");
        let Gamefinish=document.getElementById("gamefinish");
        let Answer=document.getElementById("answer");
        let Answer1=document.getElementById("answer1");
        
        document.addEventListener("DOMContentLoaded",function(){
            document.getElementById("overlay").style.display="block";
            document.getElementById("guesses").textContent="You have "+d+" incorrect guesses";
            document.getElementById("starting").style.display="block";

        })

        function start(){
        document.getElementById("theme_answer").textContent="Theme is "+getThemename()+".";
        document.getElementById("overlay").style.display="none";
        document.getElementById("starting").style.display="none";
        arr=c.replace(/[a-z]/g,'_');
        a.textContent=arr;
        // console.log(c);
        }   

        document.addEventListener("keypress",keypressed);


        function keypressed(event){
            const key_pressed=String.fromCharCode(event.charCode);
            update(key_pressed);
        }


        function update(key_pressed){
            var wordletters=c.split('');
            if(!(/^[a-zA-Z]$/.test(key_pressed))) alert("Enter alphabets only.")
            else{
                var letter=key_pressed.toLowerCase();
                if(guessedletter.includes(letter)) alert("You alredy pressed this key.")
                else{
                    guessedletter.push(letter);
                    if(wordletters.includes(letter)){
                        for(i=0;i<wordletters.length;i++){
                            if(wordletters[i]===letter){
                                arr=arr.substring(0,i)+letter+arr.substring(i+1);
                                updateDisplay();
                            }
                        }
                        if(!arr.includes('_')){
                            gamefinish();
                        }
                    }
                    else{
                        wrongletter.push(letter);
                        count++;
                        updateDisplay();
                        displayHangman();
                        if(wrongletter.length===d){
                            gameend();
                        }
                    }
                }
            }
        }

        function updateDisplay(){
            a.textContent=arr;
            b.textContent=wrongletter.join('');
        }

        function gameend(){
            overlay.style.display="block";
            Answer.textContent=getRandomWord();
            Gameend.style.display="block";
            document.removeEventListener("keypress",keypressed);
        }

        function gamefinish(){
            var ans=getRandomWord();
            overlay.style.display="block";
            Answer1.textContent=ans;
            Gamefinish.style.display="block";
            document.removeEventListener("keypress",keypressed);
        }

        function play_again(){
            overlay.style.display="none";
            Gameend.style.display="none";
            Gamefinish.style.display="none";
            window.location.href="theme.html";
        }

        function exit(){
            overlay.style.display="none";
            Gameend.style.display="none";
            Gamefinish.style.display="none";
            window.location.href="firstpage.html";
        }

        function displayHangman(){
        switch(d){
            case 7:
			    EasyModedisplay();
			    break;
		    case 6:
			    MediumModedisplay();
			    break;
		    case 5:
			    HardModedisplay();
			    break;
		    }
        }

        var stand=document.getElementById("stand");
        var noose=document.getElementById("noose");
        var head=document.getElementById("head1");
        var body=document.getElementById("body");
        var short=document.getElementById("short");
        var left_hand=document.getElementById("left_hand");
        var right_hand=document.getElementById("right_hand");
        var left_leg=document.getElementById("left_leg");
        var right_leg=document.getElementById("right_leg");


        function EasyModedisplay(){
            switch(count){
                case 1:
                    stand.style.display="block";
                    noose.style.display="block";
                    break;
                case 2:
                    stand.style.display="block";
                    noose.style.display="block";
                    head.style.display="block";
                    break;
                case 3:
                    stand.style.display="block";
                    noose.style.display="block";
                    head.style.display="block";
                    body.style.display="block";
                    short.style.display="block";
                    break;
                case 4:
                    stand.style.display="block";
                    noose.style.display="block";
                    head.style.display="block";
                    head.src="body01-head-worried.png";
                    body.style.display="block";
                    short.style.display="block";
                    left_hand.style.display="block";
                    break;
                case 5:
                    stand.style.display="block";
                    noose.style.display="block";
                    head.style.display="block";
                    head.src="body01-head-worried.png";
                    body.style.display="block";
                    short.style.display="block";
                    left_hand.style.display="block";
                    right_hand.style.display="block";
                    break;
                case 6:
                    stand.style.display="block";
                    noose.style.display="block";
                    head.style.display="block";
                    head.src="body01-head-worried.png";
                    body.style.display="block";
                    short.style.display="block";
                    left_hand.style.display="block";
                    right_hand.style.display="block";
                    left_leg.style.display="block";
                    break;
                case 7:
                    stand.style.display="block";
                    noose.style.display="block";
                    head.style.display="block";
                    head.src="body01-head-dead.png";
                    body.style.display="block";
                    short.style.display="block";
                    left_hand.style.display="block";
                    right_hand.style.display="block";
                    left_leg.style.display="block";
                    right_leg.style.display="block";
                    head.style.animationPlayState="running";
                    body.style.animationPlayState="running";
                    short.style.animationPlayState="running";
                    left_hand.style.animationPlayState="running";
                    right_hand.style.animationPlayState="running";
                    left_leg.style.animationPlayState="running";
                    right_leg.style.animationPlayState="running";
                    break;
                }
            }

        function MediumModedisplay(){
            switch(count){
                case 1:
                    stand.style.display="block";
                    noose.style.display="block";
                    head.style.display="block";
                    break;
                case 2:
                    stand.style.display="block";
                    noose.style.display="block";
                    head.style.display="block";
                    body.style.display="block";
                    short.style.display="block";
                    break;
                case 3:
                    stand.style.display="block";
                    noose.style.display="block";
                    head.style.display="block";
                    head.src="body01-head-worried.png";
                    body.style.display="block";
                    short.style.display="block";
                    left_hand.style.display="block";
                    break;
                case 4:
                    stand.style.display="block";
                    noose.style.display="block";
                    head.style.display="block";
                    head.src="body01-head-worried.png";
                    body.style.display="block";
                    short.style.display="block";
                    left_hand.style.display="block";
                    right_hand.style.display="block";
                    break;
                case 5:
                    stand.style.display="block";
                    noose.style.display="block";
                    head.style.display="block";
                    head.src="body01-head-worried.png";
                    body.style.display="block";
                    short.style.display="block";
                    left_hand.style.display="block";
                    right_hand.style.display="block";
                    left_leg.style.display="block";
                    break;
                case 6:
                    stand.style.display="block";
                    noose.style.display="block";
                    head.style.display="block";
                    head.src="body01-head-dead.png";
                    body.style.display="block";
                    short.style.display="block";
                    left_hand.style.display="block";
                    right_hand.style.display="block";
                    left_leg.style.display="block";
                    right_leg.style.display="block";
                    head.style.animationPlayState="running";
                    body.style.animationPlayState="running";
                    short.style.animationPlayState="running";
                    left_hand.style.animationPlayState="running";
                    right_hand.style.animationPlayState="running";
                    left_leg.style.animationPlayState="running";
                    right_leg.style.animationPlayState="running";
                    break;
                }
            }

        function HardModedisplay(){
            switch(count){
                case 1:
                    stand.style.display="block";
                    noose.style.display="block";
                    break;
                case 2:
                    stand.style.display="block";
                    noose.style.display="block";
                    head.style.display="block";
                    break;
                case 3:
                    stand.style.display="block";
                    noose.style.display="block";
                    head.style.display="block";
                    body.style.display="block";
                    short.style.display="block";
                    break;
                case 4:
                    stand.style.display="block";
                    noose.style.display="block";
                    head.style.display="block";
                    head.src="body01-head-worried.png";
                    body.style.display="block";
                    short.style.display="block";
                    left_hand.style.display="block";
                    right_hand.style.display="block";
                    break;
                case 5:
                    stand.style.display="block";
                    noose.style.display="block";
                    head.style.display="block";
                    head.src="body01-head-dead.png";
                    body.style.display="block";
                    short.style.display="block";
                    left_hand.style.display="block";
                    right_hand.style.display="block";
                    left_leg.style.display="block";
                    right_leg.style.display="block";
                    head.style.animationPlayState="running";
                    body.style.animationPlayState="running";
                    short.style.animationPlayState="running";
                    left_hand.style.animationPlayState="running";
                    right_hand.style.animationPlayState="running";
                    left_leg.style.animationPlayState="running";
                    right_leg.style.animationPlayState="running";
                    break;
                }
        }